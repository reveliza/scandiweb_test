<?php
// DVD class
	class DVD extends Product {
		public $type_id = 1;
		
		//Retrieving products from database using the DB connection object from class "Database"
        public function getProducts()
        {
            // Create SQL statement to get all data from DB that correspond to needed category
            $prod = "SELECT * FROM products WHERE cat_id='$this->type_id'";
            //Use connect method from Database class with SQL
            $result = $this->connect()->query($prod);
            //Num_rows get number of rows from the retrieved result
            $rowNum = $result->num_rows;
            if ($rowNum>0) {
                //If anything in the results (anything retrieved from the DB)
                while ($row = $result->fetch_assoc()){
                    //Add each row to var $data as a piece of array
                    $data_prod[]=$row;
                }
                //Return array of results
                return $data_prod;
            }
        }

		public function getCategorySpecifics()
		{
			$cat = $this->connect()->query("SELECT * FROM categories WHERE id='$this->type_id'");
			$rowNum = $cat->num_rows;
            if ($rowNum>0) {
                //If anything in the results (anything retrieved from the DB)
                while ($row = $cat->fetch_assoc()){
                    //Add each row to var $data as a piece of array
                    $data[]=$row;
                }
                //Return array of results
                return $data;
            }
		}

		public function printProducts()
		{      
			// Retrieve products from a specific category
			$results_prod = $this->getProducts();
			// Retrieve specific category attributes as an array with one element
			$category = $this->getCategorySpecifics();
	        // Set the document structure by defining a variable that will count columns in each line
			$col_num=0;
			echo "<div class='row'>";
	        //Loop through all results in the array
			foreach ($results_prod as $product) {
	            //Print attributes values of the DB data in columns, each row containing 4
				echo "<div class='col-3'>";
				echo "<input type='checkbox' class='checkbox' name='check_list[]' value='".$product['SKU']."'>";
				echo "<p>".$product['SKU']."</p>";
				echo "<p>".$product['name']."</p>";
				echo "<p>".$product['price']."$ </p>";
				// Print category attribute name, value and unit
				echo "<p>".$category[0]['attribute_name'].": ".$product['attribute_value']." ".$category[0]['attribute_unit']."</p>"."</div>";
	            // Increase current column count by one
				$col_num++;
	            // If reached 4th column in one row
				if ($col_num>3) {
	                // Finish current row and create an instance for a new row; set column count in a row to zero
					echo "</div><div class='row'>";
					$col_num=0;
				}
			}
			echo "</div>";
		}
	}

// Book class, extends methods from DVD and Product Class
	class Book extends DVD
	{
		// Overrides methods getProducts() to only get books from DB, method getCategorySpecifics() to get book attributes 
		// and method printProducts() to print each item with the appropriate category attributes
		public $type_id = 2;
	}

	class Furniture extends DVD
	{
		public $type_id = 3;
	}
?>
