<?php 
	// Class to PRINT ALL existing products
	class PrintTypes extends DVD {
		public function printAll()
		{
			$dvds = new DVD;
			$dvds->printProducts();

			$books = new Book;
			$books->printProducts();

			$furniture = new Furniture;
			$furniture->printProducts();
		}
		
	}
?>