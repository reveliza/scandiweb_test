<?php
    class Product extends Database
    {
        private $SKU;
        private $name;
        private $price;
        private $attribute_value;
        private $cat_id;
        public $type_id = 0;

        // Constructor for new Product object
        public function __construct() {
            $SKU = null;
            $name = null;
            $price = null;
            $attribute_value = null;
            $cat_id = null;
     
            $this->SKU = $SKU;
            $this->name = $name;
            $this->price = $price;
            $this->attribute_value = $attribute_value;
            $this->cat_id = $cat_id;
        }

// ADD PRODUCTS
        // Function to add new product to DB
        public function addProduct($SKU, $name, $price, $attribute_value, $cat_id)
        {
            // Set all product attributes using user input values
            $this->SKU = $SKU;
            $this->name = $name;
            $this->price = $price;
            $this->attribute_value = $attribute_value;
            $this->cat_id = $cat_id;

            $result = '';
            // If there are many attribute values (like in the case of "Furniture"), treat them as an array and iterate over them
            if(is_array($this->attribute_value)){
                //$i<3 because there can be max 3 attribute values (height, width, length)
                for ($i=0; $i<3; $i++) {
                    //if iterating over first 2 values
                    if ($i<2) {
                        //concatenate them in form "H x W x" 
                        $result = $result.$this->attribute_value[$i]."x";

                    } else {
                        //add the last value so that value of "attribute_value" forms a string of "H x W x L"
                        $result = $result.$this->attribute_value[$i];
                    }
                 }
                 //assign the resulting string to object attribute
                 $this->attribute_value = $result;
            // Probably not the best idea to assign attribute values in such way because it will be a torture to get decent data out of the DB, but this will do for the particular exercise
            }

            //If any of the fields empty, stop sending data to DB and call a warning
            if (empty($this->SKU)||empty($this->name)||empty($this->price)||empty($this->attribute_value)) {
                echo "<br>";
                if (empty($this->SKU)) {
                    //echo warning message about each field that was left empty
                    echo "<div class='alert alert-danger' role='alert'>Please insert SKU!</div>";
                }
                if (empty($this->name)) {
                    echo "<div class='alert alert-danger' role='alert'>Please insert name!</div>";
                }
                if (empty($this->price)) {
                    echo "<div class='alert alert-danger' role='alert'>Please insert price!</div>";
                }
                if (empty($this->attribute_value)) {
                    echo "<div class='alert alert-danger' role='alert'>Please insert item attributes!</div>";
                }
            } else {
                // Create the query to interact with DB
                $sql = "INSERT INTO products (SKU, name, price, attribute_value, cat_id) VALUES ('$this->SKU', '$this->name', '$this->price', '$this->attribute_value', '$this->cat_id')";
                // Send query to DB with product attributes
                $db = $this->connect()->query($sql);
                // Echo success message
                echo "<br>";
                echo "<div class='alert alert-success' role='alert'>Product added!</div>";
            }
        }

// DELETE PRODUCTS
        // Deleting items that were sent as an array from the DB
        public function deleteProducts($checked_boxes)
        {   
            // iterate over each item in the given array of checked boxes
            foreach ($checked_boxes as $id) {
                // Create seperate sql for each item
                $sql = "DELETE FROM products WHERE SKU='$id'";
                // Delete each object from DB Whose SKU corresponds to the given attribute
                $result = $this->connect()->query($sql);
            }
            // Refresh the page
            header("Refresh:0");
        }
    }
?>