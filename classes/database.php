<?php
	//Create database class to be used in the main page 
	class Database
	{
		//Private attributes
		private $DB_SERVER;
		private $DB_DATABASE;
		private $DB_USERNAME;
		private $DB_PASSWORD;

		//Function to connect to DB with the following attributes
		protected function connect()
		{
			$this->DB_SERVER = "localhost";
			$this->DB_DATABASE = "scandiweb";
			$this->DB_USERNAME = "scandiweb";
			$this->DB_PASSWORD = "scandiweb";
			
			//Creating new database connection as an object
			$database = new mysqli ($this->DB_SERVER, $this->DB_DATABASE, $this->DB_USERNAME, $this->DB_PASSWORD);
			//Returning the connection as object
			return $database;
		}
		
	}
?>