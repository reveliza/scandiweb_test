<?php
	//Include classes
	require_once '../classes/database.php';
	require_once '../classes/product.php';
    require_once '../classes/types.php';
    require_once '../classes/print.php';
?>

<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Product List</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" href="../assets/CSS/app.css">
        <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        <link href="https://fonts.googleapis.com/css?family=Roboto:400,700" rel="stylesheet">
    </head>
    <body>
    	<nav class="navbar navbar-light bg-light">
            <span class="navbar-brand mb-0 h1">Product List</span>
        </nav>
        <form action="" method="post">
            <div class="container">
                <?php 
                    $all = new PrintTypes;
                    $all->printAll();
                ?>
            </div>
		
        <span id="list_delete">
            <select name="nav_select">
                <option value="empty">Action</option>
                <option value="delete_action">Mass Delete</option>
            </select>
            <input type="submit" name="submit" value="Apply" id="delete" class="btn btn-light">  
        </span>
        <?php
            // Run PHP when submit (Apply) button for Mass Delete action clicked 
            if(isset($_POST['submit'])){
                // check if exist any checked boxes and if delete action selected from the options
                if(!empty($_POST['check_list'])&&$_POST['nav_select']=='delete_action'){
                    // Create varible containing an array of all checkbox values (each value = SKU of the respective product)
                    $checked_boxes = $_POST['check_list'];
                    // Create new list object
                    $list = new Product;
                    // Call method to delete instances of an array passed as an argument in class ProductList
                    $list->deleteProducts($checked_boxes);
                }
            }
        ?>
        </form>

		<script
		  src="https://code.jquery.com/jquery-3.3.1.min.js"
		  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
		  crossorigin="anonymous"></script>
		<script type="text/javascript" src="../assets/JS/app.js"></script>
    </body>
</html>
