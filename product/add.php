<?php
	//Include classes
require_once '../classes/database.php';
require_once '../classes/product.php';
?>

<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Product Add</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" href="../assets/CSS/app.css">
        <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        <link href="https://fonts.googleapis.com/css?family=Roboto:400,700" rel="stylesheet">
    </head>
    <body>
        <nav class="navbar navbar-light bg-light">
            <span class="navbar-brand mb-0 h1">Product Add</span>
        </nav>
        <!-- Input form -->
        <form class="add" action="" method="post">
            <!-- Product SKU input -->
            <label for="SKU">SKU</label>
            <input class="form-control" type="text" name="SKU" value="">
            <!-- Product name input -->
            <label for="name">Name</label>
            <input class="form-control" type="name" name="name" value="">
            <!-- Product price input -->
            <label for="price">Price</label>
            <input class="form-control" type="number" step=".01" min="0" name="price" value="">
            <!-- Product type input -->
            <label for="type_switcher">Type switcher</label>
            <select class="custom-select" id="type_switcher" name="cat_id">
                <option selected> </option>
                <option value="1">DVD-disc</option>
                <option value="2">Book</option>
                <option value="3">Furniture</option>
            </select>
            <!-- Field for product inputs (appears when type selected) -->
            <span id="attribute_input"></span>
            <!-- Save button -->
            <input type="submit" name="submit" value="Save" id="save" class="btn btn-light">
        </form>

        <?php
        //if form was submited by pressing Save
        if (isset($_POST['submit'])) {
            //get input data from fields
            $SKU = $_POST['SKU'];           
            $name = $_POST['name'];
            $price = $_POST['price'];
            //if attribute type is chosen and value/-s entered
            if (isset($_POST['attribute_value'])) {
                //assign value/-s to variable
                $attribute_value = $_POST['attribute_value'];
            } else {
                //else assign empty string
                $attribute_value = '';
            }
            $cat_id = $_POST['cat_id'];

            // Create new object with given attributes
            $product = new Product;
            // Call addProduct method on the object from class Database
            $product->addProduct($SKU, $name, $price, $attribute_value, $cat_id);
        }
        ?>
        <script
        src="https://code.jquery.com/jquery-3.3.1.min.js"
        integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
        crossorigin="anonymous"></script>
        <script type="text/javascript" src="../assets/JS/app.js"></script>
    </body>
</html>