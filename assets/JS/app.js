$(document).ready(function()
{
    // Function to dynamically change attribute input fields based on product type
    $('#type_switcher').change(function(){                            //focus on type switcher dropdown menu and run function every time its value changes
		var result = "";                                              //declare variable "result"
		$( "#type_switcher option:selected" ).each(function() {       //for every possible option in the dropdown
      		result += $(this).text() + " ";                           //return name of the selected option as the variable "result"
    	});
    	if (result==" ") {
    		$("#attribute_input").empty();                            //if no option selected empty the field with id "attribute_input" (DOES NOT WORK!)
    	} else {
            $("#attribute_input").load("../attributes/"+result);      //else load respective view from attributes folder
        }
	});
});