<div class="form-group" id="2">
	<label for="weight">Weight</label>
	<input class="form-control" name="attribute_value" type="number" id="book" step=".01" min="0" placeholder="kg">
	<p><small>Please input the weight of the book in KG, numbers only</small></p>
</div>